## NuxCC

NuxCC is a small compiler for a subset of the C programming language. This subset is basically C89 (without floating-point numbers and some obsolete or uncommon features) plus a few C99 features.

The compiler comes along with other tools that complete the development toolchain (assemblers, linkers, and VMs).

NuxCC is able to compile itself and also other non-trivial programs (see the tests for examples).

Currently supported targets are:

* i386
* x86_64
* NuxVM (32 & 64 bits)
* MIPS32
* ARM

## Source roadmap

| Directory/File | Contents |
| --- | --- |
| `*.c` | Core compiler |
| `src/includes` | Header files |
| `src/nuxld` | x86-(32/64), MIPS, and ARM linker |
| `src/nuxvm` | Nux VM and assembler and linker for it |
| `src/nuxdvr` | Compiler driver and .conf files |
| `src/cgen` | Code generators |
| `src/lib` | The standard C library |
| `src/tests` | Test programs |


## Documentation

See `doc/index.html` for in-depth information on usage, internals, and more.

## License

BSD license.
